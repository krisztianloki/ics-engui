A collection of scripts to generate a simple OPI from EPICS databases

Use engui_parse_db to parse databases and create the script that can be used to generate the actual OPI.

Usage:
	engui_parse_db [db1] [db2]...
if no parameters are given, stdout is read. The resulting script is printed to stdout.
Example:
	engui_parse_db wienermpod_crate.db >create_wienermpod_crate_opi

Feel free to check and modify the generated script to change the default label and the type of the widget a PV will have.

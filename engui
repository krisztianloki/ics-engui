trap 'on_exit' EXIT

function remove_intermediate_opis
{
    [[ -z "$label_opi" ]] || rm $label_opi
    [[ -z "$inppv_opi" ]] || rm $inppv_opi
    [[ -z "$outpv_opi" ]] || rm $outpv_opi
}

function on_exit
{
    remove_intermediate_opis
}

function debuglog
{
    echo "$@";
} >&2

#$engui_no_pipes
engui_path=`dirname "$0"`;
engui_path=`realpath "$engui_path"`
filename=`basename "$0"`;
engui_command="$filename";
engui_widgets="$engui_path/widgets";
engui_templates="$engui_path/templates";



if [[ "$filename" = "engui" ]]; then
    if [[ $# -lt 1 ]]; then
        echo "Usage: $engui_command <action>" >&2
        exit 1;
    fi
    task="$1";
    engui_command="$filename $task";
    shift;
else
    task="${filename##engui_}";
fi


#debuglog "DIR      : $engui_path";
#debuglog "WIDGETS  : $engui_widgets";
#debuglog "TEMPLATES: $engui_templates";
#debuglog "FILENAME : $filename";
#debuglog "CMD      : $engui_command";
#debuglog "TASK     : $task";


function do_task()
{
    debuglog "$task" "$@";

    case "$task" in
        "opi_begin")
            engui_opi_begin;
            ;;

        "add_action")
            engui_add_action "$@";
            ;;

        "add_button")
            engui_add_button "$@";
            ;;

        "add_combo")
            engui_add_combo "$@";
            ;;

        "add_led")
            engui_add_led "$@";
            ;;

        "add_menubutton")
            engui_add_menubutton "$@";
            ;;

        "add_textinput")
            engui_add_textinput "$@";
            ;;

        "add_textupdate")
            engui_add_textupdate "$@";
            ;;

        "include")
            engui_include "$@";
            ;;

        "opi_end")
            engui_opi_end;
            ;;

        *)
            echo "Unknown task: $task" >&2;
            exit 1;
            ;;
    esac
}

if [[ -z "$engui_opi" ]]; then
    engui_opi="BOY";
fi

case "$engui_opi" in
    "BOY")
        engui_opi_ext="opi";
        ;;

    "BOB")
        engui_opi_ext="bob";
        ;;

    *)
        echo "Unknown OPI type: $engui_opi" >&2
        exit 1
        ;;
esac

function parse_pair()
{
    if [[ $# -gt 2 || $# -lt 1 ]]; then
        echo "Usage: $engui_command [label] PV" >&2
        exit 1
    fi

    label="${1}";
    if [[ -z "$2" ]]; then
        label=$(echo "$label" | sed 's/.*://');
    fi
    unset outpv;
    inppv="${2:-$1}";
    label_opi=`mktemp -t gen_labelXXX.$engui_opi_ext`;
    outpv_opi=`mktemp -t gen_outpvXXX.$engui_opi_ext`;
    ln -sf $engui_widgets/Placeholder.$engui_opi_ext $outpv_opi;
    inppv_opi=`mktemp -t gen_inppvXXX.$engui_opi_ext`;
#debuglog "Label: |$label|";
#debuglog "PV:    |$inppv|";
}

function parse_triplet()
{
    if [[ $# -gt 3 || $# -lt 2 ]]; then
        echo "Usage: $engui_command [label] Output-PV Input-PV" >&2
        exit 1
    fi

    label="${1}";
    outpv="${1}";
    if [[ $# -eq 3 ]]; then
        outpv="${2:-$1}";
    else
        label=$(echo "$label" | sed 's/.*://');
    fi
    inppv="${3:-$2}";
    label_opi=`mktemp -t gen_labelXXX.$engui_opi_ext`;
    outpv_opi=`mktemp -t gen_outpvXXX.$engui_opi_ext`;
    inppv_opi=`mktemp -t gen_inppvXXX.$engui_opi_ext`;
#debuglog "Label: |$label|";
#debuglog "OUTPV: |$outpv|";
#debuglog "INPPV: |$inppv|";
}



function name_from_pv
{
    name_n=$(echo $1 | tr ' ' '_');
}

function text_from_pv
{
    text_t=$(echo $1 | tr '_' ' ');
}

function add_item_pair
{
    local magic_text="ENGUI_GENERATED_ITEMS";
    local xml_text="<!-- $magic_text -->";

    local label_opi="$1";
    local pvitem_opi="$2";
    local comment="${3:-generated}";
    comment=`echo $comment | tr -s '-'`

    if [[ "$engui_no_pipes" ]]; then
        echo "<!-- $comment label -->";
        cat $label_opi;
        echo "<!-- $comment pvitem -->";
        cat $pvitem_opi;
    else
        local template="-";
        #If stdin is a terminal then use Template-pair.opi, otherwise we use stdin
        [ -t 0 ] && template=$engui_templates/Template-pair.$engui_opi_ext;

        sed "
/$magic_text/ {
s/.*/<!-- $comment label -->/
r $label_opi
a <!-- $comment pvitem -->
r $pvitem_opi
a $xml_text
}" $template
    fi
}

function add_item_triplet
{
    local magic_text="ENGUI_GENERATED_ITEMS";
    local xml_text="<!-- $magic_text -->";

    local label_opi="$1";
    local pvitem_1_opi="$2";
    local pvitem_2_opi="$3";
    local comment="${4:-generated}";
    #comment="${comment//+[-]/-}"
    comment=`echo $comment | tr -s '-'`;

    if [[ "$engui_no_pipes" ]]; then
        echo "<!-- $comment label -->";
        cat $label_opi;
        echo "<!-- $comment pvitem -->";
        cat $pvitem_1_opi;
        echo "<!-- $comment pvitem -->";
        cat $pvitem_2_opi;
    else
        local template="-";
        #If stdin is a terminal then use Template-triplet.opi, otherwise we use stdin
        [ -t 0 ] && template=$engui_templates/Template-triplet.$engui_opi_ext;

        sed "
/$magic_text/ {
s/.*/<!-- $comment label -->/
r $label_opi
a <!-- $comment pvitem -->
r $pvitem_1_opi
a <!-- $comment pvitem -->
r $pvitem_2_opi
a $xml_text
}" $template
    fi
}



###############################################################################
###############################################################################
function engui_opi_begin
{
    sed '/^<\/display>/d' "$engui_templates"/Display.opi;
}

function engui_add_action
{
    parse_pair "$@";

    engui_create_label  "$label";
    engui_create_action "$inppv";

    engui_add_items;
}

function engui_add_button
{
    parse_triplet "$@";

    engui_create_label      "$label";
    engui_create_button     "$outpv";
    engui_create_textupdate "$inppv";

    engui_add_items;
}

function engui_add_combo
{
    parse_triplet "$@";

    engui_create_label      "$label";
    engui_create_combo      "$outpv";
    engui_create_textupdate "$inppv";

    engui_add_items;
}

function engui_add_led
{
    parse_pair "$@";

    engui_create_label      "$label";
    engui_create_led        "$inppv";

    engui_add_items;
}

function engui_add_menubutton
{
    parse_triplet "$@";

    engui_create_label      "$label";
    engui_create_menubutton "$outpv";
    engui_create_textupdate "$inppv";

    engui_add_items;
}

function engui_add_textinput
{
    parse_triplet "$@";

    engui_create_label      "$label";
    engui_create_textinput  "$outpv";
    engui_create_textupdate "$inppv";

    engui_add_items;
}

function engui_add_textupdate
{
    parse_pair "$@";

    engui_create_label      "$label";
    engui_create_textupdate "$inppv";

    engui_add_items;
}

function engui_opi_end
{
    sed 's/ENGUI_NUMBER_OF_COLUMNS/3/' "$engui_widgets"/GridLayout.opi
    echo "</display>";
}


###########################
#                         #
# engui_CREATE_ functions #
#                         #
###########################
function engui_create_action
{
    debuglog " |=Creating action for $1...";
    name_from_pv "$1";
    sed "s/WIDGETNAME/$name_n/;s/PVNAME/$1/" $engui_widgets/Action.$engui_opi_ext > $inppv_opi
}

function engui_create_button
{
    debuglog " |=Creating button for $1...";
    name_from_pv "$1";
    sed "s/WIDGETNAME/$name_n/;s/PVNAME/$1/" $engui_widgets/Button.$engui_opi_ext > $outpv_opi
}

function engui_create_label
{
    local tooltip=${outpv:-$inppv};
    debuglog " |=Creating label for $1...";
    text_from_pv "$1";
    name_from_pv "$1";
    sed "s/WIDGETNAME/$name_n/;s/ENGUI_TEXT/$text_t/;s/ENGUI_TOOLTIP/$tooltip/" $engui_widgets/Label.$engui_opi_ext > $label_opi
}

function engui_create_led
{
    debuglog " |=Creating LED for $1...";
    name_from_pv "$1";
    sed "s/WIDGETNAME/$name_n/;s/PVNAME/$1/" $engui_widgets/LED.$engui_opi_ext > $inppv_opi
}

function engui_create_menubutton
{
    debuglog " |=Creating menubutton for $1...";
    name_from_pv "$1";
    sed "s/WIDGETNAME/$name_n/;s/PVNAME/$1/" $engui_widgets/MenuButton.$engui_opi_ext > $outpv_opi
}

function engui_create_placeholder
{
    debuglog " |=Creating placeholder for $1...";
    name_from_pv "$1";
    sed "s/WIDGETNAME/$name_n/" $engui_widgets/Placeholder.$engui_opi_ext > $outpv_opi
}

function engui_create_textupdate
{
    debuglog " |=Creating textupdate for $1...";
    name_from_pv "$1";
    sed "s/WIDGETNAME/$name_n/;s/PVNAME/$1/" $engui_widgets/TextUpdate.$engui_opi_ext > $inppv_opi
}

function engui_create_combo
{
    debuglog " |=Creating combo for $1...";
    name_from_pv "$1";
    sed "s/WIDGETNAME/$name_n/;s/PVNAME/$1/" $engui_widgets/Combo.$engui_opi_ext > $outpv_opi
}

function engui_create_textinput
{
    debuglog " |=Creating textinput for $1...";
    name_from_pv "$outpv";
    sed "s/WIDGETNAME/$name_n/;s/PVNAME/$1/" $engui_widgets/TextInput.$engui_opi_ext > $outpv_opi
}

############################
#                          #
#   engui_ADD_ functions   #
#                          #
############################
function engui_add_pair
{
    debuglog " =Adding new pair for $label-$inppv";
#    create_item_pair | update_item_pair $label_opi $inppv_opi "$label-$inppv"
    add_item_pair "$label_opi" "$inppv_opi" "$label-$inppv"
}

function engui_add_triplet
{
    debuglog " =Adding new triplet for $label-${outpv:-placeholder}-$inppv";
#    create_item_triplet | update_item_triplet $label_opi $outpv_opi $inppv_opi "$label-$outpv-$inppv"
    add_item_triplet "$label_opi" "$outpv_opi" "$inppv_opi" "$label-${outpv:-placeholder}-$inppv"
}

function engui_add_items
{
    engui_add_triplet;
    debuglog "--------------------------------------------------------------------------------";
}

do_task "$@";

